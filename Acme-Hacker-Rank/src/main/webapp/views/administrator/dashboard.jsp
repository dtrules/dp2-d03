<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<spring:message code="administrator.dashboard.avg" var="avgHeader" />
<spring:message code="administrator.dashboard.min" var="minHeader" />
<spring:message code="administrator.dashboard.max" var="maxHeader" />
<spring:message code="administrator.dashboard.std" var="stdHeader" />

<spring:message code="administrator.dashboard.position.salary"
	var="salaryOfferedHeader" />
<spring:message code="administrator.dashboard.curriculas"
	var="curriculasHeader" />
<spring:message code="administrator.dashboard.results"
	var="resultsHeader" />
<spring:message code="administrator.dashboard.finders.ratio"
	var="ratioHeader" />
<spring:message code="administrator.dashboard.position.number"
	var="numberHeader" />
<spring:message code="administrator.dashboard.hacker.data"
	var="hackerHeader" />
<spring:message code="administrator.dashboard.companies.data"
	var="companiesHeader" />
<spring:message code="administrator.dashboard.hackers.data"
	var="hackersHeader" />
<spring:message code="administrator.dashboard.salaries.data"
	var="salariesHeader" />






<!--  Custom table style -->
<head>
<link rel="stylesheet" href="styles/tablas.css" type="text/css">
<link rel="stylesheet" href="styles/charts.css" type="text/css">
</head>



<!-- C level -->

<table>
	<caption>
		<jstl:out value="${salaryOfferedHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="${avgHeader}"></jstl:out></th>
		<th><jstl:out value="${minHeader}"></jstl:out></th>
		<th><jstl:out value="${maxHeader}"></jstl:out></th>
		<th><jstl:out value="${stdHeader}"></jstl:out></th>
	</tr>

	<tr>
		<td><jstl:out value="${avgSalary}"></jstl:out></td>
		<td><jstl:out value="${minSalary}"></jstl:out></td>
		<td><jstl:out value="${maxSalary}"></jstl:out></td>
		<td><jstl:out value="${stddevSalary}"></jstl:out></td>
	</tr>
</table>
<br />

<table>
	<caption>
		<jstl:out value="${numberHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="${avgHeader}"></jstl:out></th>
		<th><jstl:out value="${minHeader}"></jstl:out></th>
		<th><jstl:out value="${maxHeader}"></jstl:out></th>
		<th><jstl:out value="${stdHeader}"></jstl:out></th>
	</tr>

	<tr>
		<td><jstl:out value="${avgPositions}"></jstl:out></td>
		<td><jstl:out value="${minPositions}"></jstl:out></td>
		<td><jstl:out value="${maxPositions}"></jstl:out></td>
		<td><jstl:out value="${stddevPositions}"></jstl:out></td>
	</tr>
</table>
<br />


<table>
	<caption>
		<jstl:out value="${hackerHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="${avgHeader}"></jstl:out></th>
		<th><jstl:out value="${minHeader}"></jstl:out></th>
		<th><jstl:out value="${maxHeader}"></jstl:out></th>
		<th><jstl:out value="${stdHeader}"></jstl:out></th>
	</tr>

	<tr>
		<td><jstl:out value="${avgAppHacker}"></jstl:out></td>
		<td><jstl:out value="${minAppHacker}"></jstl:out></td>
		<td><jstl:out value="${maxAppHacker}"></jstl:out></td>
		<td><jstl:out value="${stddevAppHacker}"></jstl:out></td>
	</tr>
</table>
<br />







<!-- B level -->

<table>
	<caption>
		<jstl:out value="${curriculasHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="${avgHeader}"></jstl:out></th>
		<th><jstl:out value="${minHeader}"></jstl:out></th>
		<th><jstl:out value="${maxHeader}"></jstl:out></th>
		<th><jstl:out value="${stdHeader}"></jstl:out></th>
	</tr>

	<tr>
		<td><jstl:out value="${avgCurriculas}"></jstl:out></td>
		<td><jstl:out value="${maxCurriculas}"></jstl:out></td>
		<td><jstl:out value="${minCurriculas}"></jstl:out></td>
		<td><jstl:out value="${stddevCurriculas}"></jstl:out></td>
	</tr>
</table>
<br />

<table>
	<caption>
		<jstl:out value="${resultsHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="${avgHeader}"></jstl:out></th>
		<th><jstl:out value="${minHeader}"></jstl:out></th>
		<th><jstl:out value="${maxHeader}"></jstl:out></th>
		<th><jstl:out value="${stdHeader}"></jstl:out></th>
	</tr>

	<tr>
		<td><jstl:out value="${avgResults}"></jstl:out></td>
		<td><jstl:out value="${maxResults}"></jstl:out></td>
		<td><jstl:out value="${minResults}"></jstl:out></td>
		<td><jstl:out value="${stddevResults}"></jstl:out></td>
	</tr>
</table>
<br />

<table>
	<caption>
		<jstl:out value="${ratioHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="${ratioHeader}"></jstl:out></th>
	</tr>
	<tr>
		<td>${ratioFinders}</td>
	</tr>
</table>
<br />



<table>
	<caption>
		<jstl:out value="${companiesHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="First"></jstl:out></th>
		<th><jstl:out value="Second"></jstl:out></th>

	</tr>

	<tr>
		<td><jstl:out value="Company1"></jstl:out></td>
		<td><jstl:out value="Company2"></jstl:out></td>

	</tr>
</table>
<br />


<table>
	<caption>
		<jstl:out value="${hackersHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="First"></jstl:out></th>
		<th><jstl:out value="Second"></jstl:out></th>

	</tr>

	<tr>
		<td><jstl:out value="Hacker1"></jstl:out></td>
		<td><jstl:out value="Hacker2"></jstl:out></td>

	</tr>
</table>
<br />




<table>
	<caption>
		<jstl:out value="${salariesHeader}"></jstl:out>
	</caption>
	<tr>
		<th><jstl:out value="${avgHeader}"></jstl:out></th>
		<th><jstl:out value="${minHeader}"></jstl:out></th>
		<th><jstl:out value="${maxHeader}"></jstl:out></th>
		<th><jstl:out value="${stdHeader}"></jstl:out></th>
	</tr>

	<tr>
		<td><jstl:out value="536.56"></jstl:out></td>
		<td><jstl:out value="1"></jstl:out></td>
		<td><jstl:out value="1500"></jstl:out></td>
		<td><jstl:out value="621.9899516"></jstl:out></td>
	</tr>
</table>
<br />