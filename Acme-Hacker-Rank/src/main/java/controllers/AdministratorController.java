/*
 * AdministratorController.java
 * 
 * Copyright (C) 2019 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import security.UserAccount;
import security.UserAccountRepository;
import services.AdministratorService;
import services.ConfigurationsService;
import domain.Administrator;
import forms.AdministratorForm;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

	// Constructors -----------------------------------------------------------

	// Services -------------------------------------------------------------
	@Autowired
	private AdministratorService	administratorService;

	@Autowired
	private ConfigurationsService	configurationsService;

	@Autowired
	private UserAccountRepository	userAccountRepository;


	/* Crear administrador con objeto form */

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		AdministratorForm admin;

		try {
			admin = new AdministratorForm();
			result = new ModelAndView("administrator/create");
			result.addObject("administratorForm", admin);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute("administratorForm") @Valid final AdministratorForm administratorForm, final BindingResult binding) {
		ModelAndView result;
		Administrator admin;

		admin = this.administratorService.reconstruct(administratorForm, binding);

		if (!administratorForm.getUserAccount().getPassword().equals(administratorForm.getConfirmPassword()))
			if (LocaleContextHolder.getLocale().getLanguage().toUpperCase().contains("ES")) {
				binding.addError(new FieldError("administratorForm", "confirmPassword", administratorForm.getConfirmPassword(), false, null, null, "Las contraseņas deben coincidir"));
				return this.createEditModelAndView(administratorForm);
			}

			else {
				binding.addError(new FieldError("administratorForm", "confirmPassword", administratorForm.getConfirmPassword(), false, null, null, "Passwords must be the same"));
				return this.createEditModelAndView(administratorForm);
			}

		if (binding.hasErrors()) {
			final List<ObjectError> errors = binding.getAllErrors();
			for (final ObjectError e : errors)
				System.out.println(e.toString());
			result = new ModelAndView("administrator/create");
			result.addObject("administratorForm", administratorForm);
		} else
			try {
				final Md5PasswordEncoder encoder = new Md5PasswordEncoder();
				final UserAccount userAccount = admin.getUserAccount();
				final String password = userAccount.getPassword();
				final String hashedPassword = encoder.encodePassword(password, null);
				userAccount.setPassword(hashedPassword);
				final UserAccount ua = this.userAccountRepository.save(userAccount);
				admin.setUserAccount(ua);
				this.administratorService.save(admin);
				result = new ModelAndView("redirect:../security/login.do");
			} catch (final Throwable oops) {
				System.out.println(admin);
				System.out.println(oops.getMessage());
				System.out.println(oops.getClass());
				System.out.println(oops.getCause());
				result = this.createEditModelAndView(administratorForm);

				if (oops instanceof DataIntegrityViolationException)
					result = this.createEditModelAndView(administratorForm, "admin.duplicated.username");
				else
					result = this.createEditModelAndView(administratorForm, "admin.registration.error");
			}
		return result;

	}
	// Action-2 ---------------------------------------------------------------

	// Dashboard -----------------------------------------------------------
	@RequestMapping("/dashboard")
	public ModelAndView dashboard() {
		final ModelAndView result;

		// Queries
		final Double avgSalary = this.administratorService.avgSalary();
		final int maxSalary = this.administratorService.maxSalary();
		final int minSalary = this.administratorService.minSalary();
		final Double stddevSalary = this.administratorService.stddevSalary();

		final Double avgCurriculas = this.administratorService.avgCurriculas();
		final int maxCurriculas = this.administratorService.maxCurriculas();
		final int minCurriculas = this.administratorService.minCurriculas();
		final Double stddevCurriculas = this.administratorService.stddevCurriculas();
		final Double avgResults = this.administratorService.avgResults();
		final int maxResults = this.administratorService.maxResults();
		final int minResults = this.administratorService.minResults();
		final Double stddevResults = this.administratorService.stddevResults();
		final Double ratioFinders = this.administratorService.ratioFinders();

		final Double avgPositions = this.administratorService.avgPositions();
		final Double maxPositions = this.administratorService.maxPositions();
		final Double minPositions = this.administratorService.minPositions();
		final Double stddevPositions = this.administratorService.stddevPositions();

		final Double avgAppHacker = this.administratorService.avgAppHacker();
		final Double maxAppHacker = this.administratorService.maxAppHacker();
		final Double minAppHacker = this.administratorService.minAppHacker();
		final Double stddevAppHacker = this.administratorService.stddevAppHacker();

		result = new ModelAndView("administrator/dashboard");

		result.addObject("avgPositions", avgPositions);
		result.addObject("maxPositions", maxPositions);
		result.addObject("minPositions", minPositions);
		result.addObject("stddevPositions", stddevPositions);

		result.addObject("avgAppHacker", avgAppHacker);
		result.addObject("maxAppHacker", maxAppHacker);
		result.addObject("minAppHacker", minAppHacker);
		result.addObject("stddevAppHacker", stddevAppHacker);

		result.addObject("avgSalary", avgSalary);
		result.addObject("maxSalary", maxSalary);
		result.addObject("minSalary", minSalary);
		result.addObject("stddevSalary", stddevSalary);

		result.addObject("avgCurriculas", avgCurriculas);
		result.addObject("maxCurriculas", maxCurriculas);
		result.addObject("minCurriculas", minCurriculas);
		result.addObject("stddevCurriculas", stddevCurriculas);
		result.addObject("avgResults", avgResults);
		result.addObject("maxResults", maxResults);
		result.addObject("minResults", minResults);
		result.addObject("stddevResults", stddevResults);
		result.addObject("ratioFinders", ratioFinders);

		result.addObject("avgSalary", avgSalary);
		result.addObject("maxSalary", maxSalary);
		result.addObject("minSalary", minSalary);
		result.addObject("stddevSalary", stddevSalary);

		result.addObject("avgCurriculas", avgCurriculas);
		result.addObject("maxCurriculas", maxCurriculas);
		result.addObject("minCurriculas", minCurriculas);
		result.addObject("stddevCurriculas", stddevCurriculas);
		result.addObject("avgResults", avgResults);
		result.addObject("maxResults", maxResults);
		result.addObject("minResults", minResults);
		result.addObject("stddevResults", stddevResults);
		result.addObject("avgSalary", avgSalary);
		result.addObject("maxSalary", maxSalary);
		result.addObject("minSalary", minSalary);
		result.addObject("ratioFinders", ratioFinders);

		return result;
	}

	private ModelAndView forbiddenOpperation() {
		return new ModelAndView("redirect:/");
	}

	protected ModelAndView createEditModelAndView(final AdministratorForm administratorForm) {
		ModelAndView result;

		final Collection<String> brands = this.configurationsService.getConfiguration().getBrandNames();
		result = this.createEditModelAndView(administratorForm, null);
		result.addObject("brands", brands);

		return result;
	}

	protected ModelAndView createEditModelAndView(final AdministratorForm companyForm, final String message) {
		ModelAndView result;

		result = new ModelAndView("administrator/create");
		result.addObject("administratorForm", companyForm);
		result.addObject("message", message);

		return result;
	}

}
