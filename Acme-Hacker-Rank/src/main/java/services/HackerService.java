
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.HackerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountRepository;
import domain.CreditCard;
import domain.Curricula;
import domain.Finder;
import domain.Hacker;
import domain.SocialProfile;
import forms.HackerForm;

@Service
@Transactional
public class HackerService {

	// Manage Repository
	@Autowired
	private HackerRepository		hackerRepository;

	@Autowired(required = false)
	private Validator				validator;

	@Autowired
	private UserAccountRepository	userAccountRepository;

	@Autowired
	private CreditCardService		creditCardService;

	@Autowired
	private FolderService			folderService;


	// CRUD methods

	public Hacker create() {
		final Hacker result = new Hacker();
		final CreditCard creditCard = new CreditCard();
		final UserAccount userAccount = new UserAccount();
		final Collection<Authority> authorities = new ArrayList<Authority>();
		final Authority authority = new Authority();
		final Collection<Curricula> curricula = new ArrayList<Curricula>();
		final Finder finder = new Finder();
		final Date now = new Date(System.currentTimeMillis() - 1000);
		finder.setMoment(now);
		final Collection<SocialProfile> socialProfiles = new ArrayList<SocialProfile>();

		authority.setAuthority(Authority.HACKER);
		authorities.add(authority);
		userAccount.setAuthorities(authorities);

		result.setFinder(finder);
		result.setCurriculas(curricula);
		result.setUserAccount(userAccount);
		result.setCreditCard(creditCard);
		result.setSocialProfiles(socialProfiles);

		return result;

	}

	public Hacker save(final Hacker hacker) {
		Hacker res;
		Assert.notNull(hacker);
		res = this.hackerRepository.save(hacker);

		this.folderService.generateFolders(res.getId());

		return res;

	}

	public Hacker findOne(final int hackerId) {
		final Hacker result = this.hackerRepository.findOne(hackerId);
		Assert.notNull(result);

		return result;
	}

	public Hacker findOneNoAssert(final int hackerId) {
		final Hacker result = this.hackerRepository.findOne(hackerId);

		return result;
	}

	public Collection<Hacker> findAll() {
		final Collection<Hacker> result = this.hackerRepository.findAll();
		Assert.notNull(result);
		Assert.notEmpty(result);

		return result;
	}

	public void delete(final Hacker hacker) {
		Assert.notNull(hacker);

		this.hackerRepository.delete(hacker);
	}

	// Other business methods
	public Hacker findByPrincipal() {
		Hacker result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	/*** Reconstruct object, check validity and update binding ***/
	public Hacker reconstruct(final HackerForm hackerForm, final BindingResult binding) {
		final Hacker result = this.create();
		result.getUserAccount().setPassword(hackerForm.getUserAccount().getPassword());
		result.getUserAccount().setUsername(hackerForm.getUserAccount().getUsername());
		//		Assert.isTrue(hackerForm.getUserAccount().getPassword() == hackerForm.getConfirmPassword());

		result.getCreditCard().setBrandName(hackerForm.getCreditCard().getBrandName());
		result.getCreditCard().setCvv(hackerForm.getCreditCard().getCvv());
		result.getCreditCard().setExpiryMonth(hackerForm.getCreditCard().getExpiryMonth());
		result.getCreditCard().setExpiryYear(hackerForm.getCreditCard().getExpiryYear());
		result.getCreditCard().setHolderName(hackerForm.getCreditCard().getHolderName());
		result.getCreditCard().setNumber(hackerForm.getCreditCard().getNumber());

		result.setAddress(hackerForm.getAddress());
		result.setEmail(hackerForm.getEmail());
		result.setName(hackerForm.getName());
		result.setPhoneNumber(hackerForm.getPhoneNumber());
		result.setPhoto(hackerForm.getPhoto());
		result.setSurname(hackerForm.getSurname());
		result.setVat(hackerForm.getVat());

		result.setFinder(hackerForm.getFinder());

		this.validator.validate(result, binding);

		return result;
	}
	public Hacker reconstruct(final Hacker hacker, final BindingResult binding) {
		Hacker result = new Hacker();
		final Hacker res = new Hacker();

		if (hacker.getId() == 0)
			result = hacker;
		else {
			result = this.findOne(hacker.getId());
			final CreditCard creditCard = hacker.getCreditCard();
			res.setAddress(hacker.getAddress());
			res.setEmail(hacker.getEmail());
			res.setName(hacker.getName());
			res.setPhoneNumber(hacker.getPhoneNumber());
			res.setPhoto(hacker.getPhoto());
			res.setSurname(hacker.getSurname());
			res.setId(result.getId());
			res.setVersion(result.getVersion());
			res.setUserAccount(result.getUserAccount());
			res.setCurriculas(result.getCurriculas());
			res.setFinder(result.getFinder());
			res.setSocialProfiles(result.getSocialProfiles());

			creditCard.setBrandName(hacker.getCreditCard().getBrandName());
			creditCard.setCvv(hacker.getCreditCard().getCvv());
			creditCard.setExpiryMonth(hacker.getCreditCard().getExpiryMonth());
			creditCard.setExpiryYear(hacker.getCreditCard().getExpiryYear());
			creditCard.setHolderName(hacker.getCreditCard().getHolderName());
			creditCard.setNumber(hacker.getCreditCard().getNumber());
			res.setCreditCard(creditCard);
			res.setVat(hacker.getVat());
		}
		this.validator.validate(res, binding);

		return res;
	}
	/************************************************************************************************/

	public Hacker findByUserAccount(final UserAccount userAccount) {
		Assert.notNull(userAccount);

		Hacker result;

		result = this.hackerRepository.findByUserAccountId(userAccount.getId());

		return result;
	}

	public void checkIfHacker() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.HACKER))
				res = true;
		Assert.isTrue(res);
	}

	public Hacker findOneByUsername(final String username) {
		Assert.notNull(username);

		return this.hackerRepository.findByUserName(username);
	}

	//	public Hacker findHackerByApplicationId(final int applicationId) {
	//		return this.hackerRepository.findHackerByApplicationId(applicationId);
	//	}

	public void flush() {
		this.hackerRepository.flush();
	}

	public Hacker findHackerByCurriculaId(final int curriculaId) {
		return this.hackerRepository.findHackerByCurriculaId(curriculaId);
	}

}
