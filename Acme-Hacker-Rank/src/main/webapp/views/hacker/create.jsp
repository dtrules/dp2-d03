<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="hacker/create.do" modelAttribute="hackerForm">

	

	<%-- username--%>
	<acme:textbox code="hacker.username" path="userAccount.username" />
	<br>

	<%-- password--%>
	<acme:password code="hacker.password" path="userAccount.password" />
	<br>
	
	<%-- Confirm password--%>
	<acme:password code="hacker.ConfirmPassword" path="confirmPassword" />
	<br>

	<%-- Name --%>
	<acme:textbox code="hacker.name" path="name" />
	<br>


	<%-- Surname --%>
	<acme:textbox code="hacker.surname" path="surname" />
	<br>

	<%-- Photo --%>
	<acme:textbox code="hacker.photo" path="photo" />
	<br>

	<%-- Phone --%>
	<acme:textbox code="hacker.phoneNumber" path="phoneNumber" />
	<br>
	
	<%-- VAT --%>
	<acme:textbox code="hacker.vat" path="vat" />
	<br>

	<%-- email --%>
	<acme:textbox code="hacker.email" path="email" />
	<br>

	<%-- Address --%>
	<acme:textbox code="hacker.address" path="address" />
	<br>
	
<%-- holderName --%>
	<acme:textbox code="hacker.creditCard.holderName" path="creditCard.holderName" />
	<br>
	
	
	<%-- holderName --%>
	<acme:textbox code="hacker.creditCard.brandName" path="creditCard.brandName" />
	<br>
	
	
	<%-- Number --%>
	<acme:textbox code="hacker.creditCard.number" path="creditCard.number" />
	<br>
	
	<%-- expiryMonth --%>
	<acme:textbox code="hacker.creditCard.expiryMonth" path="creditCard.expiryMonth" />
	<br>
	
	<%-- expiryYear --%>
	<acme:textbox code="hacker.creditCard.expiryYear" path="creditCard.expiryYear" />
	<br>
	
	<%-- cvv --%>
	<acme:textbox code="hacker.creditCard.cvv" path="creditCard.cvv" />
	<br>
	
	

	
	

	
	<p><input id="field_terms" onchange="this.setCustomValidity(validity.valueMissing ? '<spring:message code="hacker.check.terms"/>' : '');" type="checkbox" required name="terms"><spring:message code="hacker.terms"/></p>

	<script type="text/javascript">

		function phoneNumberValidator() {

			var phoneNumber = document.getElementById("phone").value;

			var patternCCACPN = /^(\+[1-9][0-9]{0,2}) (\([1-9][0-9]{0,2}\)) (\d{3}\d+)/
			$;
			var patternCCPN = /^(\+[1-9][0-9]{0,2}) (\d{3}\d+)/
			$;
			var patternPN = /^(\d{3}\d+)/
			$;

			if (patternCCACPN.test(phoneNumber))
				return true;
			else if (patternCCPN.test(phoneNumber))
				return true;
			else if (patternPN.test(phoneNumber))
				return true;
			else
				return confirm('<spring:message code="hacker.confirm"/>');
		}
		
	</script>

	<%-- Buttons --%>
	<input type="submit" name="save"
		value="<spring:message code="hacker.save"/>"
		onClick="javascript: return phoneNumberValidator()"/>
	<acme:cancel code="hacker.cancel" url="/"/>
	
</form:form>

<script>

  document.getElementById("field_terms").setCustomValidity("<spring:message code="hacker.check.terms"/>");

</script>