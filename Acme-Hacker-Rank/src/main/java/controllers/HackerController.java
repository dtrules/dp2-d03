
package controllers;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import security.UserAccount;
import security.UserAccountRepository;
import services.ConfigurationsService;
import services.CreditCardService;
import services.HackerService;
import domain.Configurations;
import domain.CreditCard;
import domain.Hacker;
import forms.HackerForm;

@Controller
@RequestMapping("/hacker")
public class HackerController extends AbstractController {

	@Autowired
	private HackerService			hackerService;

	@Autowired
	private ConfigurationsService	configurationsService;

	@Autowired
	private CreditCardService		creditCardService;

	@Autowired
	private UserAccountRepository	userAccountRepository;


	@ExceptionHandler(TypeMismatchException.class)
	public ModelAndView handleMismatchException(final TypeMismatchException oops) {
		return new ModelAndView("redirect:/");
	}

	// List ------------------------------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Hacker> bros;

		try {
			bros = this.hackerService.findAll();
			result = new ModelAndView("hacker/list");
			result.addObject("hackers", bros);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	// Register ------------------------------------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		HackerForm bro;

		try {
			bro = new HackerForm();
			result = new ModelAndView("hacker/create");
			result.addObject("hackerForm", bro);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	// Save the new hacker ------------------------------------------------------------------------------------

	// Edit ------------------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView result;
		final Hacker hacker;

		try {
			hacker = this.hackerService.findByPrincipal();

			result = new ModelAndView("hacker/edit");
			result.addObject("hacker", hacker);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute("hackerForm") @Valid final HackerForm hackerForm, final BindingResult binding) {
		ModelAndView result;
		Hacker hacker;

		hacker = this.hackerService.reconstruct(hackerForm, binding);
		final CreditCard creditCard = hacker.getCreditCard();
		final Configurations configurations = this.configurationsService.getConfiguration();
		final Collection<String> brands = configurations.getBrandNames();

		if (!hackerForm.getUserAccount().getPassword().equals(hackerForm.getConfirmPassword()))
			if (LocaleContextHolder.getLocale().getLanguage().toUpperCase().contains("ES")) {
				binding.addError(new FieldError("hackerForm", "confirmPassword", hackerForm.getConfirmPassword(), false, null, null, "Las contraseņas deben coincidir"));
				return this.createEditModelAndView(hackerForm);
			}

			else {
				binding.addError(new FieldError("hackerForm", "confirmPassword", hackerForm.getConfirmPassword(), false, null, null, "Passwords must be the same"));
				return this.createEditModelAndView(hackerForm);
			}

		if (binding.hasErrors()) {
			final List<ObjectError> errors = binding.getAllErrors();
			for (final ObjectError e : errors)
				System.out.println(e.toString());

			result = new ModelAndView("hacker/create");
			result.addObject("hackerForm", hackerForm);
		}

		else
			try {
				final Md5PasswordEncoder encoder = new Md5PasswordEncoder();
				final UserAccount userAccount = hacker.getUserAccount();
				final String password = userAccount.getPassword();
				final String hashedPassword = encoder.encodePassword(password, null);
				userAccount.setPassword(hashedPassword);
				final UserAccount ua = this.userAccountRepository.save(userAccount);
				hacker.setUserAccount(ua);

				this.hackerService.save(hacker);
				result = new ModelAndView("redirect:../security/login.do");
			} catch (final Throwable oops) {
				System.out.println(hacker);
				System.out.println(oops.getMessage());
				System.out.println(oops.getClass());
				System.out.println(oops.getCause());
				result = this.createEditModelAndView(hackerForm);

				if (oops instanceof DataIntegrityViolationException)
					result = this.createEditModelAndView(hackerForm, "hacker.duplicated.username");
				else
					result = this.createEditModelAndView(hackerForm, "hacker.registration.error");
			}
		return result;
	}
	// SAVE ------------------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@ModelAttribute("hacker") final Hacker prune, final BindingResult binding) {
		ModelAndView result;
		final Hacker hacker;

		hacker = this.hackerService.reconstruct(prune, binding);

		if (binding.hasErrors()) {
			final List<ObjectError> errors = binding.getAllErrors();
			for (final ObjectError e : errors)
				System.out.println(e.toString());

			result = this.editModelAndView(hacker);
			result.addObject("hacker", prune);
		}

		else
			try {
				this.hackerService.save(hacker);
				result = new ModelAndView("redirect:/");
			} catch (final Throwable oops) {
				System.out.println(hacker);
				System.out.println(oops.getMessage());
				System.out.println(oops.getClass());
				System.out.println(oops.getCause());
				result = this.editModelAndView(hacker, "hacker.registration.error");
			}
		return result;
	}

	// Ancillary methods -----------------------------------------------------------------------
	protected ModelAndView createEditModelAndView(final HackerForm hackerForm) {
		ModelAndView result;

		result = this.createEditModelAndView(hackerForm, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final HackerForm hackerForm, final String message) {
		ModelAndView result;

		result = new ModelAndView("hacker/create");
		result.addObject("hackerForm", hackerForm);
		result.addObject("message", message);

		return result;
	}

	protected ModelAndView editModelAndView(final Hacker hacker) {
		ModelAndView result;

		result = this.editModelAndView(hacker, null);

		return result;
	}

	protected ModelAndView editModelAndView(final Hacker hacker, final String message) {
		ModelAndView result;

		result = new ModelAndView("hacker/edit");
		result.addObject("hacker", hacker);
		result.addObject("message", message);

		return result;
	}

	private ModelAndView forbiddenOpperation() {
		return new ModelAndView("redirect:/");
	}
}
