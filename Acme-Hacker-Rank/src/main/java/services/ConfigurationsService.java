
package services;

import domain.Actor;
import domain.Administrator;
import domain.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Configurations;
import repositories.ConfigurationsRepository;

@Service
@Transactional
public class ConfigurationsService {

	// Manage Repository
	@Autowired
	private ConfigurationsRepository	configurationsRepository;


	// Supporting services
	@Autowired
	private ActorService actorService;

	// CRUD methods
	public Configurations getConfiguration() {
		Configurations result = this.configurationsRepository.findAll().get(0);

		Assert.notNull(result);

		return result;
	}

	public Configurations save(final Configurations config) {
		Assert.notNull(config);

		Actor principal;

		// Principal must be a Administrator
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);

		this.checkConfiguration(config);

		return this.configurationsRepository.save(config);
	}

	public void flush(){
		this.configurationsRepository.flush();
	}

	// Other business methods ---------------------------------------------------------

	public void checkConfiguration(Configurations config){
		boolean check = true;

		if(config.getBanner()==null || config.getSystemName() == null || config.getWelcomeMessageEn() == null || config.getWelcomeMessageEs() == null){
			check=false;
		}

		Assert.isTrue(check);
	}
}
