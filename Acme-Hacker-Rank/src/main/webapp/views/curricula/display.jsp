<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-------------------------------------- POSITION DATA ---------------------------------------->

<h2><spring:message code="curricula.positionData" /></h2>


<display:table name="positionsData" id="row" pagesize="5"
	requestURI="${requestUri}" class="displaytag">


	<spring:message code="curricula.positionData.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" />
	

	
		
		
        <display:column>
              <a href="positionData/list.do?curriculaId=${curriculaId}">
                <spring:message code="curricula.positionData.display"/>
              </a>
        </display:column>
        
        
             
       

	<display:column>
              <a href="positionData/edit.do?positionDataId=${row.id}">
                <spring:message code="curricula.positionData.edit"/>
              </a>
        </display:column>
	
	       <display:column>
        
        <input type="submit" name="delete" value="<spring:message code="curricula.delete" />"
				onclick="javascript: relativeRedir('positionData/delete.do?positionDataId=${row.id}');" />
                </display:column>
        
	
	</br>

		

</display:table>

 <a href="positionData/create.do?curriculaId=${curriculaId}">
                <spring:message code="curricula.positionData.create"/>
              </a>


<!-------------------------------------- EDUCATION DATA ---------------------------------------->

<h2><spring:message code="curricula.educationData" /></h2>


<display:table name="educationsData" id="row" pagesize="5"
	requestURI="${requestUri}" class="displaytag">


	<spring:message code="curricula.educationData.institution" var="institutionHeader" />
	<display:column property="institution" title="${institutionHeader}" />
	

	
		
		
        <display:column>
              <a href="educationData/show.do?educationDataId=${row.id}">
                <spring:message code="curricula.educationData.display"/>
              </a>
        </display:column>
        
                <display:column>
        
       
        
             

	<display:column>
              <a href="educationData/edit.do?educationDataId=${row.id}">
                <spring:message code="curricula.educationData.edit"/>
              </a>
        </display:column>
        
         <input type="submit" name="delete" value="<spring:message code="curricula.delete" />"
				onclick="javascript: relativeRedir('educationData/delete.do?educationDataId=${row.id}');" />
                </display:column>
	
	
	

</display:table>

 <a href="educationData/create.do?curriculaId=${curriculaId}">
                <spring:message code="curricula.educationData.create"/>
              </a>


<h2><spring:message code="curricula.miscellaneousData" /></h2>


<display:table name="miscellaneousData" id="row" pagesize="5"
	requestURI="${requestUri}" class="displaytag">


        

	<display:column>
              <a href="miscellaneousData/edit.do?miscellaneousDataId=${row.id}">
                <spring:message code="curricula.miscellaneousData.edit"/>
              </a>
        </display:column>
        
        <display:column>
              <a href="miscellaneousData/show.do?miscellaneousDataId=${row.id}">
                <spring:message code="curricula.miscellaneousData.display"/>
              </a>
        </display:column>
	
	
	

</display:table>


<h2><spring:message code="curricula.personalData" /></h2>


<display:table name="personalData" id="row" pagesize="5"
	requestURI="${requestUri}" class="displaytag">


	
        
       

	<display:column>
              <a href="personalData/edit.do?personalDataId=${row.id}">
                <spring:message code="curricula.personalData.edit"/>
              </a>
        </display:column>
        
        <display:column>
              <a href="personalData/show.do?personalDataId=${row.id}">
                <spring:message code="curricula.personalData.display"/>
              </a>
        </display:column>
	
	
	

</display:table>

