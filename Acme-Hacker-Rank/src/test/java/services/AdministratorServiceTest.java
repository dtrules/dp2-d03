
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class AdministratorServiceTest extends AbstractTest {

	// Service under test---------------------------
	@Autowired
	private AdministratorService	administratorService;


	// Templates -------------------------------------------------------

	protected void testAvgSalary(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST AVG SALARY --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Double avgSalary = this.administratorService.avgSalary();
			final String print = avgSalary.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	protected void testMinSalary(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST MIN SALARY --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Integer minSalary = this.administratorService.minSalary();
			final String print = minSalary.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	protected void testMaxSalary(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST MAX SALARY --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Integer maxSalary = this.administratorService.maxSalary();
			final String print = maxSalary.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	protected void testStddevSalary(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST STDDEV SALARY --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Double stddevSalary = this.administratorService.stddevSalary();
			final String print = stddevSalary.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	protected void testAvgCurriculas(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST AVG CURRICULAS--------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Double avgCurriculas = this.administratorService.avgCurriculas();
			final String print = avgCurriculas.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	protected void testMinCurriculas(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST MIN CURRICULAS--------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Integer minCurriculas = this.administratorService.minCurriculas();
			final String print = minCurriculas.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	protected void testMaxCurriculas(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST MAX CURRICULAS --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Integer maxCurriculas = this.administratorService.maxCurriculas();
			final String print = maxCurriculas.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	protected void testStddevCurriculas(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST STDDEV CURRICULAS  --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Double stddevCurriculas = this.administratorService.stddevCurriculas();
			final String print = stddevCurriculas.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	protected void testAvgResults(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST AVG RESULTS --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Double avgResults = this.administratorService.avgResults();
			final String print = avgResults.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	protected void testMinResults(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST MIN RESULTS --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Integer minResults = this.administratorService.minResults();
			final String print = minResults.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	protected void testMaxResults(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST MAX RESULTS --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Integer maxResults = this.administratorService.maxResults();
			final String print = maxResults.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	protected void testStddevResults(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST STDDEV RESULTS --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Double stddevResults = this.administratorService.stddevResults();
			final String print = stddevResults.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	protected void testRatioFinders(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			System.out.println("------------------------------------TEST RATIO FINDERS --------------------------------------------------");

			super.authenticate(username);

			@SuppressWarnings("unused")
			final Double ratioFinders = this.administratorService.ratioFinders();
			final String print = ratioFinders.toString();
			System.out.println(print);

			super.unauthenticate();
			System.out.println("----------------------------PASSED-------------------------------\r");
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	//DRIVERS

	@Test
	public void driverAvgSalary() {

		final Object testingData[][] = {

			// sentence coverage 100%

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"hacker1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testAvgSalary((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverMinSalary() {

		final Object testingData[][] = {

			// sentence coverage 100%

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"hacker1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testMinSalary((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverMaxSalary() {

		final Object testingData[][] = {

			// sentence coverage 100%

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"hacker1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testMaxSalary((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverStddevSalary() {

		final Object testingData[][] = {

			// sentence coverage 100%

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"hacker1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testStddevSalary((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverAvgCurriculas() {

		final Object testingData[][] = {

			// sentence coverage 100%

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"hacker1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testAvgCurriculas((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverMinCurriculas() {

		final Object testingData[][] = {

			// sentence coverage 100%

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"hacker1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testMinCurriculas((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverMaxCurriculas() {

		final Object testingData[][] = {

			// sentence coverage 100%

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"hacker1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testMaxCurriculas((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverStddevCurriculas() {

		final Object testingData[][] = {

			// sentence coverage 100%

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"hacker1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testStddevCurriculas((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverAvgResults() {

		final Object testingData[][] = {

			// sentence coverage 100%

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"hacker1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testAvgResults((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverMinResults() {

		final Object testingData[][] = {

			// sentence coverage 100%

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"hacker1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testMinResults((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverMaxResults() {

		final Object testingData[][] = {

			// sentence coverage 100%

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"hacker1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testMaxResults((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverStddevResults() {

		final Object testingData[][] = {

			// sentence coverage 100%

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"hacker1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testStddevResults((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverRatioFindersResults() {

		final Object testingData[][] = {

			// sentence coverage 100%

			// a) Mostrar información de la dashboard como admin
			{
				"admin", null,
			},
			// a) Mostrar información de la dashboard como admin
			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
			{
				"hacker1", IllegalArgumentException.class,
			},

		};

		for (int i = 0; i < testingData.length; i++)
			this.testRatioFinders((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

}
