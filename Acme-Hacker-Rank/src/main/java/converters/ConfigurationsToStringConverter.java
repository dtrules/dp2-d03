
package converters;

import domain.Company;
import domain.Configurations;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import sun.security.krb5.Config;

@Component
@Transactional
public class ConfigurationsToStringConverter implements Converter<Configurations, String> {

	@Override
	public String convert(final Configurations configurations) {
		String result;
		if (configurations == null)
			result = null;
		else
			result = String.valueOf(configurations.getId());
		return result;
	}

}
