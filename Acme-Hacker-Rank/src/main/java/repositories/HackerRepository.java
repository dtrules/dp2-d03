
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Hacker;

@Repository
public interface HackerRepository extends JpaRepository<Hacker, Integer> {

	@Query("select a from Hacker a where a.id = ?1")
	Hacker findById(int id);

	@Query("select a from Hacker a where a.userAccount.username = ?1")
	Hacker findByUserName(String username);

	@Query("select a from Hacker a where a.userAccount.id = ?1")
	Hacker findByUserAccountId(int id);

	@Query("select h from Hacker h join h.curriculas p where p.id = ?1")
	Hacker findHackerByCurriculaId(int curriculaId);

	//	@Query("select max(h.  from Hacker h")
	//	Double maxApplicationsPerHacker();

	//	@Query("select h from Hacker h join h.applications a where a.id = ?1")
	//	Hacker findHackerByApplicationId(int applicationId);
}
