<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="company" id="row" requestURI="company/display.do" class="displaytag">

	<spring:message code="company.commercialName" var="commercialNameHeader" />
	<display:column property="commercialName" title="${commercialNameHeader}" />
	
	<spring:message code="company.address" var="addressHeader" />
	<display:column property="address" title="${addressHeader}" />
	
	<spring:message code="company.email" var="emailHeader" />
	<display:column property="email" title="${emailHeader}" />
	
	<spring:message code="company.photo" var="photoHeader" />
	<display:column property="photo" title="${photoHeader}" />
	
	<spring:message code="company.phoneNumber" var="phoneNumberHeader" />
	<display:column property="phoneNumber" title="${phoneNumberHeader}" />

</display:table>



<security:authorize access="isAnonymous()">
	<acme:cancel code="position.cancel" url="/position/anonymousList.do" />
</security:authorize>

<security:authorize access="isAuthenticated()">
	<acme:cancel code="position.cancel" url="/position/list.do" />
</security:authorize>
