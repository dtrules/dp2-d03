
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Administrator;
import domain.Company;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {

	@Query("select admin from Administrator admin where admin.userAccount.id = ?1")
	Administrator findByUserAccountId(int id);

	@Query("select c from Company c")
	Collection<Company> getCompaniesWithMorePositions();

	@Query("select avg(p.salaryOffered) from Position p")
	Double avgSalary();

	@Query("select max(p.salaryOffered) from Position p")
	int maxSalary();

	@Query("select min(p.salaryOffered) from Position p")
	int minSalary();

	@Query("select stddev(p.salaryOffered) from Position p")
	Double stddevSalary();

	@Query("select avg(h.curriculas.size) from Hacker h")
	Double avgCurriculas();

	@Query("select max(h.curriculas.size) from Hacker h")
	int maxCurriculas();

	@Query("select min(h.curriculas.size) from Hacker h")
	int minCurriculas();

	@Query("select stddev(h.curriculas.size) from Hacker h")
	Double stddevCurriculas();

	@Query("select avg(f.positions.size) from Finder f")
	Double avgResults();

	@Query("select max(f.positions.size) from Finder f")
	int maxResults();

	@Query("select min(f.positions.size) from Finder f")
	int minResults();

	@Query("select stddev(f.positions.size) from Finder f")
	Double stddevResults();

	@Query("select 1.0*count(f)/(select count(f) from Finder f where f.positions.size = 0) from Finder f where f.positions.size > 0")
	Double ratioFinders();

	@Query("select max(1.0*(select count(p) from Position p where p.company.id = c.id)) from Company c")
	Double maxPositions();

	@Query("select avg(1.0*(select count(p) from Position p where p.company.id = c.id)) from Company c")
	Double avgPositions();

	@Query("select min(1.0*(select count(p) from Position p where p.company.id = c.id)) from Company c")
	Double minPositions();

	@Query("select stddev(1.0*(select count(p) from Position p where p.company.id = c.id)) from Company c")
	Double stddevPositions();

	//Administrator applications per hacker dashboard data

	@Query("select max(1.0*(select count(a) from Application a where a.hacker.id = h.id)) from Hacker h")
	Double maxAppHacker();

	@Query("select min(1.0*(select count(a) from Application a where a.hacker.id = h.id)) from Hacker h")
	Double minAppHacker();

	@Query("select avg(1.0*(select count(a) from Application a where a.hacker.id = h.id)) from Hacker h")
	Double avgAppHacker();

	@Query("select stddev(1.0*(select count(a) from Application a where a.hacker.id = h.id)) from Hacker h")
	Double stddevAppHacker();

	//	//Best and worst positions in terms of salary
	//	@Query("select p from Position p where p.salaryOffered = max(p.salaryOffered) from Position")

}
